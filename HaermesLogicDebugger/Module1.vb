﻿Imports System.Reflection
Imports DevExpress.Data.Filtering
Imports DevExpress.Xpo
Imports Haermes.Module

Public Module Module1
    Public logicPath As String = "C:\Users\effendyt\Desktop\Data\Source\pta15.2\bin\Debug\"
    Public logicFile As String = "HaermesPTA"
    Public assemblyName As String = "HaermesPTA"
    Public className As String = "PTABase"
    Public connectionString As String = "Data Source=WFR-BTM-004\SQLSERVER2017;User=sa;Pwd=123456;Initial Catalog=PTA;"
    Private _logicAssembly As Assembly
    Public ReadOnly Property Assembly As Assembly
        Get
            If _logicAssembly Is Nothing Then
                _logicAssembly = Assembly.LoadFrom(
           logicPath & "\" &
           logicFile & ".dll")
            End If
            Return _logicAssembly
        End Get
    End Property

    Sub Compute(ByVal session As Session)
        Dim employeeCode As String = "121201009"
        Dim periodSummaryCode As String = "201802"

        Dim objPayroll As Payroll = session.FindObject(Of Payroll)(GroupOperator.And(New BinaryOperator("Employee.Code", employeeCode),
                                                                                                                         New BinaryOperator("PeriodPay.PeriodSummary.Code", periodSummaryCode)))
        If objPayroll IsNot Nothing Then
            Dim a001 As Double = ExecuteFormula.PayrollExecute(objPayroll, "A001")
            Console.WriteLine(a001)
        End If
    End Sub


    Sub Main()
        Dim session As Session = New Session()
        session.ConnectionString = Module1.connectionString
        Haermes.Module.GlobalVar.PubConnectionString = session.ConnectionString
        Compute(session)
        Console.ReadLine()
    End Sub

End Module
