﻿Imports System.Reflection
Imports Haermes.Module

Public Class ExecuteFormula

    Public Shared Function PayrollExecute(ByVal payroll As Payroll,
                                                           ByVal payrollComponent As String) As Double

        Try
            Dim objAssmbly As Assembly = Module1.Assembly
            Dim objType As H11BusinessLogicCompoment =
            System.Activator.CreateInstance(
            objAssmbly.GetType(Module1.assemblyName &
             ".PC" & Module1.className))

            Dim paramTypes(0) As Type
            paramTypes(0) = GetType(Haermes.Module.Payroll)
            Dim objMethod As MethodInfo = objType.GetType().GetMethod("F_" & payrollComponent, paramTypes)

            Dim parameters(0) As Object
            parameters(0) = payroll

            Return CDbl(objMethod.Invoke(objType, parameters))
        Catch ex As Exception
            Throw ex
        End Try

    End Function

End Class
